const crypto = require('crypto');
const S = require('string');
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

function capitalize(s) {
  return S(s).capitalize().s;
}

const schema = new Schema({
  _id: {type: String, required: true},
  firstName: {type: String, required: true, trim: true, set: capitalize},
  lastName: {type: String, required: true, uppercase: true, trim: true},
  email: {type: String, required: false, lowercase: true, trim: true},
  roles: [{type: String, required: true, lowercase: true, trim: true}],
  salt: {type: String},
  passwordDigest: {type: String}
});

schema.index({firstName: 'text', lastName: 'text', email: 'text'}, {name: 'text_search'});

schema.methods.setPassword = function(password) {
  this.salt = uuid.v4();

  const sha = crypto.createHash('sha256');
  sha.update(this.salt);
  sha.update('\n');
  sha.update(password);

  this.passwordDigest = sha.digest('hex');
};

schema.methods.verifyPassword = function(password) {
  const sha = crypto.createHash('sha256');
  sha.update(this.salt);
  sha.update('\n');
  sha.update(password);

  return this.passwordDigest == sha.digest('hex');
};

module.exports = schema;