const mongoose = require('mongoose');
const S = require('string');

const settings = require('../settings');

const connection = mongoose.createConnection(settings.mongoUrl);
const db = connection.useDb('aurelia');

class Database {
  constructor() {
    this.Person = Database.create('person');
  }

  static create(schemaName) {
    return db.model(
        Database.getModelName(schemaName),
        require(`./schemas/${schemaName}`),
        Database.getCollectionName(schemaName));
  }

  static getModelName(name) {
    return S(name).camelize().capitalize().s;
  }

  static getCollectionName(name) {
    return S(name).camelize().s;
  }
}

module.exports = Database;