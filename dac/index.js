const co = require('co');

const Database = require('./Database');

const dac = new Database();
module.exports = dac;

// HACK: set up someone to log in as
co(function *() {
  let defaultUser = yield dac.Person.findById('default');
  if (defaultUser) {
    return;
  }

  defaultUser = yield dac.Person.create({
    _id: 'default',
    firstName: 'Joe',
    lastName: 'Bloggs',
    email: 'joe@home.com',
    roles: ['admin']
  });
  defaultUser.setPassword('demo');

  yield defaultUser.save();
}).catch(console.error);