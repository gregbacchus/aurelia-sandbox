# Aurelia Demo Sandbox with Authentication

## Getting Started

To run this project you need to have Node.JS >= v4 and MongoDB installed on localhost and running.

#### Set up packages

```
npm install
cd public
jspm install
gulp bundle
cd ..
```

And then run

```
npm start
```

Enjoy