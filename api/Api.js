const _ = require('underscore');
const Router = require('koa-router');

const Auth = require('./Auth');

const DEFAULT_QUERY_SKIP = 0;
const DEFAULT_QUERY_LIMIT = 100;
const ONE_MORE = 1;
const BEGINNING = 0;
const END_MINUS_ONE = -1;

class Api {
  constructor(collection, parent, path) {
    this.collection = collection;

    this.router = new Router();
    this.createRoutes();
    parent.use(path, this.router.routes());
  }

  createRoutes() {
    this.router.get('/', Auth.authenticate, this.fixContext(this.getMany));
    this.router.get('/:id', Auth.authenticate, this.fixContext(this.getOne));
    this.router.post('/', Auth.authenticate, this.fixContext(this.insertOne));
    this.router.put('/:id', Auth.authenticate, this.fixContext(this.updateOne));
    this.router.delete('/:id', Auth.authenticate, this.fixContext(this.deleteOne));
  }

  fixContext(func) {
    const self = this;
    return function *() {
      yield func.call(self, this);
    };
  }

  extractQuery(ctx) {
    return ctx.query.query
        ? JSON.parse(ctx.query.query)
        : {};
  }

  tidyOutput(document) {
    return _.omit(document, '__v');
  }

  *getMany(ctx) {
    const query = this.extractQuery(ctx);
    const skip = ctx.query.skip || DEFAULT_QUERY_SKIP;
    const limit = ctx.query.limit || DEFAULT_QUERY_LIMIT;
    const documents = yield this.collection.find(query)
        .skip(skip)
        .limit(limit + ONE_MORE);
    const more = documents.length > limit;
    ctx.body = {
      results: _.map(more ? documents.slice(BEGINNING, END_MINUS_ONE) : documents, (document) =>
          this.tidyOutput(document.toObject())
      ),
      more
    };
  }

  *getOne(ctx) {
    const query = {_id: ctx.params.id};
    const document = yield this.collection.findOne(query);
    ctx.body = this.tidyOutput(document.toObject());
  }

  *validateInsert(insert) {
    console.log(insert);
  }

  *insertOne(ctx) { // eslint-disable-line no-unused-vars
    // validate
    // sanitize
    // metadata
    // save
  }

  *validateUpdate(update) {
    console.log(update);
  }

  *updateOne(ctx) { // eslint-disable-line no-unused-vars
    // validate
    // sanitize
    // metadata
    // audit
    // save
  }

  *deleteOne(ctx) {
    const query = {_id: ctx.params.id};
    const update = {$set: {isDeleted: true}};
    // todo: metadata
    // todo: audit
    ctx.body = yield this.collection.updateOne(query, update);
  }
}

module.exports = Api;