const _ = require('underscore');

const Api = require('./Api');
const dac = require('../dac');

class Person extends Api {
  constructor(parent, path) {
    super(dac.Person, parent, path);
  }

  extractQuery(ctx) {
    const filter = ctx.query.filter;
    return filter
        ? {$text: {$search: filter}}
        : super.extractQuery(ctx);
  }

  tidyOutput(document) {
    return _.omit(document, '__v', 'password', 'salt', 'passwordDigest');
  }
}

module.exports = Person;