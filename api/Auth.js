const _ = require('underscore');
const Router = require('koa-router');
const debug = require('debug')('patrol:api:auth');
const fs = require('fs');
const resolve = require('path').resolve;
const jwt = require('jsonwebtoken');
const passport = require('koa-passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const dac = require('../dac');

const privateKey = fs.readFileSync(resolve(__dirname, '../certs/auth'), {encoding: 'UTF8'}); // eslint-disable-line no-sync
const publicKey = fs.readFileSync(resolve(__dirname, '../certs/auth.pub'), {encoding: 'UTF8'}); // eslint-disable-line no-sync, no-unused-vars

const MILLIS_PER_SECOND = 1000;

class Auth {
  constructor(parent, path) {
    this.router = new Router();

    this.router.get('/', function *() {
      this.body = 'api/auth';
    });

    this.router.post('/signin', this.signIn);
    this.router.get('/me', Auth.authenticate, this.me);

    parent.use(path, this.router.routes());
  }

  *signIn() {
    const email = this.request.body.email.toLowerCase();
    const password = this.request.body.password;
    const person = yield dac.Person.findOne({email});

    if (!person) {
      this.body = {
        message: 'Email address not found.'
      };
      return;
    }

    if (!person.verifyPassword(password)) {
      this.body = {
        message: 'Incorrect password.'
      };
      return;
    }

    const now = new Date();
    const payload = {
      sub: person._id,
      roles: person.roles,
      iat: Math.round(now.getTime() / MILLIS_PER_SECOND)
    };
    const token = jwt.sign(payload, privateKey, {algorithm: 'HS256'});

    this.body = {token};
  }

  static get passportStrategy() {
    const options = {
      jwtFromRequest: ExtractJwt.fromAuthHeader(),
      secretOrKey: privateKey,
      algorithms: ['HS256'],
      authScheme: 'Bearer'
    };
    return new JwtStrategy(options, (payload, done) => {
      debug(payload);
      dac.Person.findById(payload.sub, (err, person) => {
        if (err) {
          done(err, false);
        } else if (person) {
          done(null, person.toObject());
        } else {
          done(null, false);
          // or you could create a new account
        }
      });
    });
  }

  static get authenticate() {
    return passport.authenticate('jwt', {session: false});
  }

  *me() {
    this.body = _.omit(this.req.user, '__v', 'password', 'salt', 'passwordDigest');
  }
}

module.exports = Auth;