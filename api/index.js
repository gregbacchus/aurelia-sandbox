const Router = require('koa-router');
const os = require('os');

const STATUS_SERVER_ERROR = 500;

class Api {
  constructor(app, path) {
    this.router = new Router({
      prefix: path
    });

    this.router.use(function *(next) {
      try {
        yield next;
      } catch (err) {
        this.status = err.status || STATUS_SERVER_ERROR;
        this.body = {error: err.name, message: err.message};
      }
    });

    this.router.get('/.info', function *() {
      this.body = {
        host: os.hostname(),
        date: new Date().toISOString(),
        freeRAM: os.freemem()
      };
    });

    new (require('./Auth'))(this.router, '/auth');
    new (require('./Person'))(this.router, '/person');

    app.use(this.router.routes());
  }
}

module.exports = Api;