const resolve = require('path').resolve;
const koa = require('koa');
const serveStatic = require('koa-static');
const compress = require('koa-compress');
const logger = require('koa-logger');
const passport = require('koa-passport');
const bodyParser = require('koa-bodyparser');

const app = module.exports = koa();

// use a real logger in production
// hide the logger during tests because it's annoying
if (app.env !== 'production' && app.env !== 'test') app.use(logger());

app.use(compress());
app.use(bodyParser());

passport.use(require('./api/Auth').passportStrategy);
app.use(passport.initialize());

// always serve the public directory
app.use(serveStatic(resolve(__dirname, 'public')));

new (require('./api'))(app, '/api');

module.exports = app;
