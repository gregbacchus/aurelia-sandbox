'use strict';

const gulp = require('gulp');
const bundler = require('aurelia-bundler');

const bundles = {
  'dist/aurelia': {
    includes: [
      'aurelia-framework',
      'aurelia-bootstrapper',
      'aurelia-fetch-client',
      'aurelia-router',
      'aurelia-animator-css',
      'aurelia-templating-binding',
      'aurelia-templating-resources',
      'aurelia-templating-router',
      'aurelia-loader-default',
      'aurelia-history-browser',
      'aurelia-logging-console',
      'aurelia-auth',
      'aurelia-http-client',
      'bootstrap',
      'bootstrap/css/bootstrap.css!text'
    ],
    options: {
      inject: true,
      minify: true,
      rev: true
    }
  }
};

const config = {
  force: true,                    // Force overwrite bundle file if already exists. Default false
  baseURL: '.',                   // `baseURL of the application`
  configPath: './config.js',      // `config.js` path. Must be within `baseURL`
  bundles
};

gulp.task('unbundle', () => bundler.unbundle(config));

gulp.task('bundle', ['unbundle'], () => bundler.bundle(config));