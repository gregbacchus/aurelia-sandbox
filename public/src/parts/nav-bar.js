import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {Router} from 'aurelia-router';

@inject(Router, AuthService)
export class NavBar {
  constructor(router, auth) {
    this.router = router;
    this.auth = auth;
  }

  get allowedRoutes() {
    if(this.auth.isAuthenticated()) return this.router.navigation;

    return this.router.navigation.filter(r => !r.config.auth);
  }
}