import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';

@inject(AuthService)
export class Auth {
  constructor(auth) {
    this.auth = auth;

    this.me = undefined;
    if (this.auth.isAuthenticated()) {
      this.loadMe();
    }
  };

  email = '';
  password = '';

  loadMe() {
    return this.auth.getMe()
        .then(me => {
          this.me = me;
        })
        .catch(err => {
          console.error(err);
        });
  }

  get isAuthenticated() {
    return this.me && this.auth.isAuthenticated();
  }

  signIn() {
    return this.auth.login(this.email, this.password)
        .then(response => {
          console.log('success logged', response);
          this.loadMe();
          this.email = '';
          this.password = '';
        })
        .catch(err => {
          // TODO leave sign in popup open
          console.log('sign in failure', err);
          this.password = '';
        });
  };

  signOut() {
    return this.auth.logout('/#/')
        .then(response => {
          this.me = undefined;
        })
        .catch(err => {
          console.log('sign out failure', err);
        });
  };

  authenticate(name) {
    return this.auth.authenticate(name, false, null)
        .then((response)=> {
          console.log("auth response " + response);
        });
  }
}