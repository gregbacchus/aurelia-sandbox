export class Error {
  code = undefined;
  messages = {
    401: 'You must be signed in to view this page. Please sign in and try again.',
    404: 'The page that you are looking for could not be found.'
  };

  activate(params) {
    this.code = params && params.code || '404';
    this.message = this.messages[this.code] || 'Code: ' + this.code;
  }
}
