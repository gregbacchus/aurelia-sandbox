import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {Router} from 'aurelia-router';

@inject(Router, HttpClient)
export class People {
  constructor(router, http) {
    this.router = router;
    this.http = http;
  }

  filter = '';
  sort = 'lastName';
  sortDirection = 'ascending';
  list = [];

  activate() {
    this.loadList();
  }

  clearFilter() {
    this.filter = '';
    this.loadList();
  }

  loadList() {
    this.http.fetch(`/api/person?filter=${this.filter}`)
        .then(response => response.json())
        .then(response => {
          this.list = response.results;
        })
        .catch(err => {
          // TODO error handling
        });
  }

  viewDetails(personId) {
    console.log(personId);
    this.router.navigateToRoute('person', {id: personId})
  }
}