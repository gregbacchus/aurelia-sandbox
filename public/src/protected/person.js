export class Person {
  firstName = 'John';
  lastName = 'Doe';
  landPone = '';
  mobilePhone = '';
  roles = [];

  activate() {
    // TODO load actual person
  }

  get fullName() {
    return `${this.firstName} ${this.lastName && this.lastName.toUpperCase()}`.trim();
  }

  updateDetails() {

  }

  updatePassword() {

  }
}