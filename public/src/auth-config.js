export default {
  signupRedirect: '/signin',
  loginUrl: '/api/auth/signin',
  signupUrl: '/api/auth/signup',
  profileUrl: '/api/auth/me',
  unlinkUrl: '/api/auth/unlink/',
  tokenPrefix: 'auth',
  loginRedirect: '/#/'
}