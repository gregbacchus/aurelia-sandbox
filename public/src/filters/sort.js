export class SortValueConverter {
  toView(array, propertyName, direction) {
    if (!propertyName || !direction) return array;

    const factor = direction === 'ascending' ? 1 : -1;
    return array
        .slice(0)
        .sort((a, b) => {
          return (a[propertyName] == b[propertyName]) ? 0
              : (a[propertyName] > b[propertyName] ? 1 : -1) * factor
        });
  }
}