export class ContainsValueConverter {
  toView(array, value) {
    return array.indexOf(value) >= 0;
  }
}