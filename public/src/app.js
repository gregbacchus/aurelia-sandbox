import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import HttpClientConfig from 'aurelia-auth/app.httpClient.config';
import {Redirect} from 'aurelia-router';
import {FetchConfig} from 'aurelia-auth';
import {AuthService} from 'aurelia-auth';

@inject(Router, HttpClientConfig, FetchConfig)
export class App {
  constructor(router, httpClientConfig, fetchConfig) {
    this.router = router;
    this.httpClientConfig = httpClientConfig;
    this.fetchConfig = fetchConfig;
  }

  configureRouter(config, router) {
    config.title = 'Home';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.map([
      {route: [''], name: 'welcome', moduleId: 'public/welcome', nav: false, title: 'Welcome'},
      {route: ['error/:code'], name: 'error', moduleId: 'public/error', nav: false, title: 'Error'},
      {route: ['about'], name: 'about', moduleId: 'public/about', nav: true, title: 'About'},
      {route: ['person/:id'], name: 'person', moduleId: 'protected/person', nav: false, title: 'Person'},
      {route: ['people'], name: 'people', moduleId: 'protected/people', nav: true, auth: true, title: 'People'}
    ]);
    config.mapUnknownRoutes('error');

    this.router = router;
  }

  activate() {
    this.httpClientConfig.configure();
    this.fetchConfig.configure();
  }
}

@inject(AuthService)
class AuthorizeStep {
  constructor(auth) {
    this.auth = auth;
  }

  run(navigationInstruction, next) {
    if (navigationInstruction.getAllInstructions().some(i => i.config.auth)) {
      if (!this.auth.isAuthenticated()) {
        return next.cancel(new Redirect('error/401'));
      }
    }

    return next();
  }
}